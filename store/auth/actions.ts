import { ActionTree } from 'vuex';
import defaultState from './state';
import { loginService } from '~/entity/auth/services/login.service'

export default {
	async login({ commit }, dto) {
		await commit('reset')
		await loginService(this.$axios, {
			...dto,
			onSuccess: (data) => {
				this.$cookies.set('token', data.token)
				this.$cookies.set('user', JSON.stringify(data))
				window.location.reload()
			},
			onError: (e) => {
				console.error(e)
				this.$toast.error('Incorrect username or password')
			},
		})
	},
	async logout() {
		this.$cookies.remove('token')
		this.$cookies.remove('user')
		window.location.reload()
	},
} as ActionTree<ReturnType<typeof defaultState>, ReturnType<typeof defaultState>>;
