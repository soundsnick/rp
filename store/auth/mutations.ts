import defaultState from './state'

export default {
	set(state: ReturnType<typeof defaultState>, payload: any) {
		state.data = payload
	},
	setError(state: ReturnType<typeof defaultState>, payload: any) {
		state.error = payload
	},
	reset(state: ReturnType<typeof defaultState>) {
		state = defaultState()
	},
}
