import defaultState from './state';

export default {
	getItem(state: ReturnType<typeof defaultState>) {
		return state.data
	},
}
