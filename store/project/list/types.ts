import { ProjectModel } from "~/entity/project/models/ProjectModel";

export default interface StateDto { 
    data: ReadonlyArray<ProjectModel> | null
}