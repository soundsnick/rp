import { ActionTree } from 'vuex';
import StateDto from "./types";
import { getAllProjectsService } from '~/entity/project/services/get-all-projects.service';

export default {
    async fetchList({ commit }) {
      await getAllProjectsService(this.$axios, { set: (payload: StateDto['data']) => commit('set', payload) });
    }
} as ActionTree<StateDto, StateDto>;