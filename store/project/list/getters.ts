import { GetterTree } from 'vuex';
import StateDto from "./types"

export default {
    getList(state) {
      return state.data
    }
} as GetterTree<StateDto, StateDto>;