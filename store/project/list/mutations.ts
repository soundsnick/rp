import { MutationTree } from 'vuex';
import StateDto from "./types"

export default {
    set(state, payload) {
      state.data = payload;
    }
} as MutationTree<StateDto>;