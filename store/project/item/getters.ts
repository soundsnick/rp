import { GetterTree } from 'vuex';
import StateDto from "./types"

export default {
    getItem(state) {
      return state.data
    }
} as GetterTree<StateDto, StateDto>;