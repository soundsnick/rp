import { ProjectModel } from "~/entity/project/models/ProjectModel";

export default interface StateDto { 
    data: ProjectModel | null,
    processing: boolean,
}