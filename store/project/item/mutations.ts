import { MutationTree } from 'vuex';
import StateDto from "./types"

export default {
    set(state, payload) {
      state.data = payload;
    },
    addTheme(state, payload) {
      if (state.data) {
        state.data.themes.push(payload);
      }
    },
    deleteTheme(state, payload) {
      if (state.data) {
        state.data.themes.splice(payload, 1);
      }
    },
    setId(state, payload) {
      if (state.data) {
        state.data.id = payload;
      }
    },
    setName(state, payload) {
      if (state.data) {
        state.data.name = payload;
      }
    },
    setProcessing(state, payload) {
      state.processing = payload;
    }
} as MutationTree<StateDto>;