import { ActionTree } from 'vuex';
import StateDto from "./types";
import { getProjectByIdService } from '~/entity/project/services/get-project-by-id.service';
import { createProjectService } from '~/entity/project/services/create-project.service';
import { deleteProjectService } from '~/entity/project/services/delete-project.service';
import { patchProjectService } from '~/entity/project/services/patch-project.service';
import { processMentionsService } from '~/entity/project/services/process-mentions.service';

export default {
    async fetchItem({ commit }, payload) {
      await getProjectByIdService(this.$axios, { id: payload, set: (payload: StateDto['data']) => commit('set', payload) });
    },
    async createItem({ dispatch }, payload) {
      await createProjectService(
        this.$axios, 
        { 
          data: payload, 
          onSuccess: () => {
            dispatch('project/list/fetchList', null, { root: true });
            this.$router.push('/');
          }, 
          onError: (error: Error) => alert('Something went wrong') 
        }
      );
    },
    async patchItem({ dispatch, state }, payload) {
      await patchProjectService(
        this.$axios, 
        { 
          data: payload, 
          onSuccess: () => {
            dispatch('fetchItem', payload.id);
            alert("Updated");
          }, 
          onError: (error: Error) => alert('Something went wrong') 
        }
      );
    },
    async deleteItem({ dispatch }, payload) {
      await deleteProjectService(
        this.$axios, 
        { 
          ...payload, 
          onSuccess: () => {
            dispatch('project/list/fetchList', null, { root: true });
          }, 
          onError: (error: Error) => alert('Something went wrong') 
        }
      );
    },
    async processMentions({ commit }, { payload, onSuccess }) {
      commit('setProcessing', true);
      await processMentionsService(this.$axios, {
        data: payload,
        onSuccess: () => {
          commit('setProcessing', false);
          alert('Successfully sent to processing');
          onSuccess();
        },
        onError: () => {
          commit('setProcessing', false);
          alert('Something went wrong');
        }
      })
    }
} as ActionTree<StateDto, StateDto>;