import { Middleware } from "@nuxt/types"

const authMiddleware: Middleware = function ({ $cookies, route, redirect }) {
	if ($cookies.get('token')) {
		if (route.path === '/auth/login') {
			return redirect('/')
		}
	} else if (route.path !== '/auth/login') {
		return redirect('/auth/login')
	}
};

export default authMiddleware;