import https from 'https'

export default function ({ $axios, $cookies }, inject) {
	const tokenCookie = $cookies.get('token');

	const api = $axios.create({
		headers: {
			'Content-Type': 'application/json',
			accept: 'application/json',
			...(tokenCookie ? { Authorization: `Token ${tokenCookie}` } : {}),
		},
	});

	api.setBaseURL(process.env.API_URL);

	const agent = new https.Agent({
		rejectUnauthorized: false,
	});
	api.onRequest((config) => {
		config.httpsAgent = agent

		if (process.env.NODE_ENV !== 'production') {
			console.log('Making request to ' + config.url)
		}
	});

	if (process.env.NODE_ENV !== 'production') {
		api.onError((e) => {
			console.error(e)
		})
	}

	inject('axios', api);
}
