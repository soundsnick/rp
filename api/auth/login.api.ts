import { NuxtAxiosInstance } from "@nuxtjs/axios";

export interface AuthRequestDTO {
    username: string;
    password: string;
};

export interface AuthResponseDTO {
    token: string;
};

export const loginApi = async (api: NuxtAxiosInstance, dto: AuthRequestDTO) => api.post(`/dashboard/login/`, dto)
