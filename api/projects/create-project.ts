import { NuxtAxiosInstance } from "@nuxtjs/axios";
import { ProjectModel } from "~/entity/project/models/ProjectModel";

interface RequestDto extends ProjectModel {}

export const createProject = (api: NuxtAxiosInstance, dto: RequestDto) => api.post(`/api/v1/neural/`, dto);