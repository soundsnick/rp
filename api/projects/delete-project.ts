import { NuxtAxiosInstance } from "@nuxtjs/axios";
import { ProjectModel } from "~/entity/project/models/ProjectModel";

interface RequestDto extends Pick<ProjectModel, 'id'> {}

export const deleteProject = (api: NuxtAxiosInstance, dto: RequestDto) => api.delete(`/api/v1/neural/${dto.id}/`);