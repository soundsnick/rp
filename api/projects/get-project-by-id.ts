import { NuxtAxiosInstance } from "@nuxtjs/axios";
import { ProjectModel } from "~/entity/project/models/ProjectModel";

interface RequestDto extends Pick<ProjectModel, 'id'> {}

interface ResponseDto extends ProjectModel {}

export const getProjectById = (api: NuxtAxiosInstance, dto: RequestDto) => api.get<ResponseDto>(`/api/v1/neural/${dto.id}`);