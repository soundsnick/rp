import { NuxtAxiosInstance } from "@nuxtjs/axios";
import { ProjectModel } from "~/entity/project/models/ProjectModel";

interface RequestDto extends ProjectModel {}

export const patchProject = (api: NuxtAxiosInstance, { dto }: { dto: RequestDto }) => api.patch(`/api/v1/neural/`, dto);