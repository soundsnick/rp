import { NuxtAxiosInstance } from "@nuxtjs/axios";

interface RequestDto extends FormData {}

export const processMentions = (api: NuxtAxiosInstance, dto: RequestDto) => api.post(`/process_mentions/`, dto);