import { NuxtAxiosInstance } from "@nuxtjs/axios";
import { ProjectModel } from "~/entity/project/models/ProjectModel";

interface ResponseDto extends ReadonlyArray<ProjectModel> {}

export const getAllProjects = (api: NuxtAxiosInstance) => api.get<ResponseDto>(`/api/v1/neural/all`);