export type ServiceRequestDto<RequestDTO, ResponseDTO> = {
    data: RequestDTO;
    onSuccess: (data: ResponseDTO) => void;
    onError: (e: Error) => void;
}