import { Toasted } from 'vue-toasted'
declare module 'vuex/types/index' {
  interface Store<S> {
    $toast: Toasted,
  }
}
