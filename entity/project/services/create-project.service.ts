import { getProjectById } from "~/api/projects/get-project-by-id";
import { ProjectModel } from "../models/ProjectModel";
import { NuxtAxiosInstance } from "@nuxtjs/axios";
import { createProject } from "~/api/projects/create-project";

interface Dto {
    data: ProjectModel,
    onError: (error: Error) => void;
    onSuccess: () => void;
}

export const createProjectService = async (api: NuxtAxiosInstance, dto: Dto) => {
    try {
        await createProject(api, dto.data);
        dto.onSuccess();
    } catch(e) {
        dto.onError(e as Error);
    }
};