import { NuxtAxiosInstance } from "@nuxtjs/axios";
import { getAllProjects } from "~/api/projects/get-all-projects";

interface Dto {
    set: (payload: any) => void;
}

export const getAllProjectsService = async (api: NuxtAxiosInstance, dto: Dto) => {
    const res = await getAllProjects(api);
    dto.set(res.data);
};