import { ProjectModel } from "../models/ProjectModel";
import { NuxtAxiosInstance } from "@nuxtjs/axios";
import { deleteProject } from "~/api/projects/delete-project";

interface Dto extends Pick<ProjectModel, 'id'> {
    onError: (error: Error) => void;
    onSuccess: () => void;
}

export const deleteProjectService = async (api: NuxtAxiosInstance, dto: Dto) => {
    try {
        await deleteProject(api, dto);
        dto.onSuccess();
    } catch(e) {
        dto.onError(e as Error);
    }
};