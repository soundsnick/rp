import { NuxtAxiosInstance } from "@nuxtjs/axios";
import { processMentions } from "~/api/projects/process-mentions";

interface Dto {
    data: FormData,
    onError: (error: Error) => void;
    onSuccess: () => void;
}

export const processMentionsService = async (api: NuxtAxiosInstance, dto: Dto) => {
    try {
        await processMentions(api, dto.data);
        dto.onSuccess();
    } catch(e) {
        dto.onError(e as Error);
    }
};