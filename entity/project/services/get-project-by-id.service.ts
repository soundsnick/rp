import { getProjectById } from "~/api/projects/get-project-by-id";
import { ProjectModel } from "../models/ProjectModel";
import { NuxtAxiosInstance } from "@nuxtjs/axios";

interface Dto extends Pick<ProjectModel, 'id'> {
    set: (payload: any) => void;
}

export const getProjectByIdService = async (api: NuxtAxiosInstance, dto: Dto) => {
    const res = await getProjectById(api, { id: dto.id });
    dto.set(res.data);
};