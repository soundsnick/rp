import { ProjectModel } from "../models/ProjectModel";
import { NuxtAxiosInstance } from "@nuxtjs/axios";
import { patchProject } from "~/api/projects/patch-project";

interface Dto {
    data: ProjectModel,
    onError: (error: Error) => void;
    onSuccess: () => void;
}

export const patchProjectService = async (api: NuxtAxiosInstance, dto: Dto) => {
    try {
        await patchProject(api, { dto: dto.data });
        dto.onSuccess();
    } catch(e) {
        dto.onError(e as Error);
    }
};