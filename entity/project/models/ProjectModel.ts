import { ThemeModel } from "./ThemeModel";

export type ProjectModel = {
    id: number;
    name: string;
    themes: Array<ThemeModel>;
}