import { NuxtAxiosInstance } from "@nuxtjs/axios"
import { AuthRequestDTO, AuthResponseDTO, loginApi } from "~/api/auth/login.api"
import { ServiceRequestDto } from "~/shared/services.types"

interface RequestDto extends ServiceRequestDto<AuthRequestDTO, AuthResponseDTO> {};

export const loginService = async (api: NuxtAxiosInstance, dto: RequestDto) => {
	try {
		const res = await loginApi(api, dto.data)
		return dto.onSuccess(res.data)
	} catch (e) {
		return dto.onError(e as Error);
	}
}
